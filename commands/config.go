package commands

import (
	"flag"

	"bitbucket.org/harrowio/harrow-cli/config"
)

type Config struct {
	args   *config.Arguments
	local  *config.Config
	global *config.Config
	view   View
}

func NewConfig(local, global *config.Config, view View) *Config {
	flags := flag.NewFlagSet("", flag.ContinueOnError)
	flags.String("global", "false", "write to global configuration file")
	args := config.NewArguments("config", flags, []string{})

	return &Config{
		view:   view,
		args:   args,
		local:  local,
		global: global,
	}
}

func (self *Config) Run(args []string) int {
	self.args.Parse(args)

	n := self.args.NArg()
	switch n {
	case 2:
		return self.set(self.args.Arg(0), self.args.Arg(1))
	case 1:
		return self.get(self.args.Arg(0))
	case 0:
		return 1
	}

	return 0
}

func (self *Config) set(key, val string) int {
	dst := (*config.Config)(nil)
	out := (config.ConfigStore)(nil)
	err := (error)(nil)

	if self.args.Get("global") == "false" {
		dst = self.local
		out, err = config.Local()
	} else {
		dst = self.global
		out, err = config.Global()
	}

	if err != nil {
		self.view.Error(err)
		return 1
	}

	if val == "" {
		dst.Unset(key)
	} else {
		dst.Set(key, val)
	}

	if err := config.Save(dst, out); err != nil {
		self.view.Error(err)
		return 1
	}

	return 0
}

func (self *Config) get(key string) int {
	lookup := config.NewFirstMatch(
		self.local,
		self.global,
	)

	if !lookup.IsSet(key) {
		return 1
	}

	val := lookup.Get(key)
	self.view.Display(val)
	return 0
}

func (self *Config) Help() string {
	return `config [--global=false] KEY [VALUE]

Writes or reads KEY from persistent configuration.

If VALUE is provided, KEY is set to VALUE in the local configuration file.

If --global is set to any value other than "false", only the global
configuration file is written.

If KEY is not set in the local configuration file, the global
configuration file is considered.`
}

func (self *Config) Synopsis() string {
	return `read or write configuration`
}
