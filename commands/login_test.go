package commands

import (
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
)

func init() {
	config.Local = config.TempConfigFile("local")
	config.Global = config.TempConfigFile("global")
}

func TestLogin_returnsASession(t *testing.T) {
	be := backend.NewMock()
	be.RespondTo("POST", "/sessions", &backend.Response{
		Subject: map[string]interface{}{
			"uuid":     "0fcbcd54-e2d1-4be5-ab02-1ebc6e9961f2",
			"userUuid": "5092f2dd-50af-4bd7-bdf6-7027b69bbf4d",
			"valid":    true,
		},
		Error: nil,
	})
	params := StaticParameters{
		"email":    "user@example.com",
		"password": "very-secure-password",
	}

	cmd := NewLogin(be, params, config.NewConfig(), nil)
	session, err := cmd.Exec()
	if err != nil {
		t.Fatal(err)
	}

	if session == nil {
		t.Fatal("session = nil; want not nil")
	}
}

func TestLogin_failsIfBackendReturnsAnError(t *testing.T) {
	be := backend.NewMock()
	params := StaticParameters{
		"email":    "user@example.com",
		"password": "very-secure-password",
	}
	cmd := NewLogin(be, params, config.NewConfig(), nil)
	session, err := cmd.Exec()
	if err == nil {
		t.Errorf("Expected an error")
	}

	if session != nil {
		t.Errorf("session = %#v; want nil", session)
	}
}

func TestLogin_bindsSessionFromResponse(t *testing.T) {
	be := backend.NewMock()
	subject := map[string]interface{}{
		"uuid":         "61741818-dc89-46ba-bb53-53190495eb77",
		"userUuid":     "2d75c7d3-d7fb-4c87-8505-2d7a9ef07127",
		"createdAt":    "2015-05-13 13:04:10.374984355+03:00",
		"userAgent":    "harrow-cli",
		"clientAdress": "127.0.0.1",
		"valid":        true,
	}
	be.RespondTo("POST", "/sessions", &backend.Response{
		Subject: subject,
		Error:   nil,
	})
	params := StaticParameters{
		"email":    "user@example.com",
		"password": "very-secure-password",
	}
	cmd := NewLogin(be, params, config.NewConfig(), nil)
	session, err := cmd.Exec()
	if err != nil {
		t.Error(err)
	}

	if got, want := session.Uuid, subject["uuid"]; got != want {
		t.Errorf("session.Uuid = %q; want %q", got, want)
	}

}

func TestLogin_setsUserIdOnConfig(t *testing.T) {
	be := backend.NewMock()
	subject := map[string]interface{}{
		"uuid":         "61741818-dc89-46ba-bb53-53190495eb77",
		"userUuid":     "2d75c7d3-d7fb-4c87-8505-2d7a9ef07127",
		"createdAt":    "2015-05-13 13:04:10.374984355+03:00",
		"userAgent":    "harrow-cli",
		"clientAdress": "127.0.0.1",
		"valid":        true,
	}
	be.RespondTo("POST", "/sessions", &backend.Response{
		Subject: subject,
		Error:   nil,
	})
	params := StaticParameters{
		"email":    "user@example.com",
		"password": "very-secure-password",
	}

	cfg := config.NewConfig()

	cmd := NewLogin(be, params, cfg, nil)
	if _, err := cmd.Exec(); err != nil {
		t.Fatal(err)
	}

	if got, want := cfg.Get("user.uuid"), subject["userUuid"]; got != want {
		t.Errorf("cfg.Get(%q) = %q; want %q", "user.uuid", got, want)
	}
}

func TestLogin_setsSessionIdOnConfig(t *testing.T) {
	be := backend.NewMock()
	subject := map[string]interface{}{
		"uuid":         "61741818-dc89-46ba-bb53-53190495eb77",
		"userUuid":     "2d75c7d3-d7fb-4c87-8505-2d7a9ef07127",
		"createdAt":    "2015-05-13 13:04:10.374984355+03:00",
		"userAgent":    "harrow-cli",
		"clientAdress": "127.0.0.1",
		"valid":        true,
	}
	be.RespondTo("POST", "/sessions", &backend.Response{
		Subject: subject,
		Error:   nil,
	})
	params := StaticParameters{
		"email":    "user@example.com",
		"password": "very-secure-password",
	}

	cfg := config.NewConfig()

	cmd := NewLogin(be, params, cfg, nil)
	if _, err := cmd.Exec(); err != nil {
		t.Fatal(err)
	}

	if got, want := cfg.Get("session.uuid"), subject["uuid"]; got != want {
		t.Errorf("cfg.Get(%q) = %q; want %q", "session.uuid", got, want)
	}
}

func TestLogin_persistsConfig(t *testing.T) {
	be := backend.NewMock()
	subject := map[string]interface{}{
		"uuid":         "61741818-dc89-46ba-bb53-53190495eb77",
		"userUuid":     "2d75c7d3-d7fb-4c87-8505-2d7a9ef07127",
		"createdAt":    "2015-05-13 13:04:10.374984355+03:00",
		"userAgent":    "harrow-cli",
		"clientAdress": "127.0.0.1",
		"valid":        true,
	}
	be.RespondTo("POST", "/sessions", &backend.Response{
		Subject: subject,
		Error:   nil,
	})
	params := StaticParameters{
		"email":    "user@example.com",
		"password": "very-secure-password",
	}

	cfg := config.NewConfig()

	cmd := NewLogin(be, params, cfg, nil)
	if _, err := cmd.Exec(); err != nil {
		t.Fatal(err)
	}

	localConfigFile, err := config.Local()
	if err != nil {
		t.Fatal(err)
	}

	reloadedConfig := config.NewConfig()
	if err := config.Load(reloadedConfig, localConfigFile); err != nil {
		t.Fatal(err)
	}

	if got, want := reloadedConfig.Get("session.uuid"), subject["uuid"]; got != want {
		t.Errorf("reloadedConfig.Get(%q) = %q; want %q", "session.uuid", got, want)
	}
}
