package commands

import (
	"fmt"
	"sync"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/interactions"
	"bitbucket.org/harrowio/harrow-cli/model"
)

type ListOrganizations struct {
	backend backend.Interface
	params  Parameters
	output  Output
}

func NewListOrganizations(backend backend.Interface, params Parameters, output Output) *ListOrganizations {
	return &ListOrganizations{
		backend: backend,
		params:  params,
		output:  output,
	}
}

func (self *ListOrganizations) Exec() ([]*model.Organization, error) {
	userUuid := self.params.Get("user.uuid")
	ix := interactions.NewListUserOrganizations(self.backend)
	res := ix.Run(&interactions.ListUserOrganizationsArguments{
		UserUuid: userUuid,
	})
	if res.Error != nil {
		return nil, res.Error
	}

	wg := sync.WaitGroup{}
	organizations := []*model.Organization{}
	for _, item := range res.Collection {
		subject := item["subject"].(map[string]interface{})
		organization, err := model.NewOrganization(subject)
		if err != nil {
			return organizations, err
		}

		organizations = append(organizations, organization)
		wg.Add(1)
		go self.loadOrganizationProjects(&wg, organization)
	}
	wg.Wait()

	return organizations, nil
}

func (self *ListOrganizations) loadOrganizationProjects(wg *sync.WaitGroup, organization *model.Organization) {
	defer wg.Done()

	ix := interactions.NewListOrganizationProjects(self.backend)
	res := ix.Run(&interactions.ListOrganizationProjectsArguments{
		OrganizationUuid: organization.Uuid,
	})

	if res.Error != nil {
		self.output.Error(fmt.Sprintf(
			"list-organization-projects: %s",
			res.Error,
		))
	}

	for _, item := range res.Collection {
		subject := item["subject"].(map[string]interface{})
		project, err := model.NewProject(subject)
		if err != nil {
			self.output.Error(fmt.Sprintf("list-organization-projects: %s", err))
		}

		organization.Projects = append(organization.Projects, project)
	}
}

func (self *ListOrganizations) Run(args []string) int {
	organizations, err := self.Exec()
	if err != nil {
		self.output.Error(err.Error())
		return 1
	}

	for _, organization := range organizations {
		line := fmt.Sprintf("%-15s %s %s", "organization", organization.Uuid, organization.Name)
		self.output.Info(line)
		for _, project := range organization.Projects {
			line := fmt.Sprintf("%-15s %s %s", "project", project.Uuid, project.Name)
			self.output.Info(line)
		}
	}

	return 0
}

func (self *ListOrganizations) Help() string {
	return `list-organizations

Lists all organizations the user is a member of.`
}

func (self *ListOrganizations) Synopsis() string {
	return `list your organizations`
}
