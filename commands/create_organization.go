package commands

import (
	"flag"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
	"bitbucket.org/harrowio/harrow-cli/interactions"
	"bitbucket.org/harrowio/harrow-cli/model"
)

type CreateOrganization struct {
	backend backend.Interface
	params  Parameters
	args    *config.Arguments
	output  Output
}

func NewCreateOrganization(backend backend.Interface, params Parameters, input config.Asker, output Output) *CreateOrganization {
	flags := flag.NewFlagSet("", flag.ContinueOnError)
	flags.String("name", "", "name of the organization")
	flags.Bool("public", true, "organization will be publicly visible")
	flags.String("github-login", "", "GitHub username for this organization")

	args := config.NewArguments("create-organization", flags, []string{})

	fromUser := config.NewFromUser(input, output)
	fromUser.Only("name")

	return &CreateOrganization{
		backend: backend,
		params: config.NewFirstMatch(
			args,
			config.NewPrefix("create-organization", params),
			fromUser,
		),
		args:   args,
		output: output,
	}
}

func (self *CreateOrganization) Exec() (*model.Organization, error) {
	args := &interactions.CreateOrganizationArguments{
		Name:        self.params.Get("name"),
		GithubLogin: self.params.Get("github-login"),
	}
	if public := self.params.Get("public"); public != "" && public != "false" {
		args.Public = true
	}

	ix := interactions.NewCreateOrganization(self.backend)
	res := ix.Run(args)
	if res.Error != nil {
		return nil, res.Error
	}

	organization, err := model.NewOrganization(res.Subject)
	if err != nil {
		return nil, err
	}

	return organization, nil
}

func (self *CreateOrganization) Run(args []string) int {
	self.args.Parse(args)
	organization, err := self.Exec()
	if err != nil {
		self.output.Error(err.Error())
		return 1
	}

	self.output.Info("Organization-Id: " + organization.Uuid)

	return 0
}

func (self *CreateOrganization) Help() string {
	return `create-organization [--github-login=user@example.com] [--public=true] --name=organization-name

Creates a new organization on Harrow.

Newly created organizations default to being publicly visible.  Specify --public to ochange this.

If --name is not provided, the user is asked to provide a name.
`
}

func (self *CreateOrganization) Synopsis() string {
	return `create a new organization`
}
