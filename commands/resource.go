package commands

import "fmt"

type Resource struct {
	name   string
	plural string
}

func NewResource(name, plural string) *Resource {
	return &Resource{name: name, plural: plural}
}

func (self *Resource) PathTo(id string) string   { return fmt.Sprintf("/%s/%s", self.plural, id) }
func (self *Resource) Param(name string) string  { return fmt.Sprintf("%s.%s", self.name, name) }
func (self *Resource) Name() string              { return self.name }
func (self *Resource) Prefix(with string) string { return fmt.Sprintf("%s-%s", with, self.name) }
func (self *Resource) Suffix(with string) string { return fmt.Sprintf("%s-%s", self.name, with) }
