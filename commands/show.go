package commands

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
	"bitbucket.org/harrowio/harrow-cli/interactions"
	"bitbucket.org/harrowio/harrow-cli/view"
)

type Show struct {
	backend  backend.Interface
	resource *Resource
	view     *view.ForFormat
	params   Parameters
	args     *config.Arguments
}

func NewShow(backend backend.Interface, resource *Resource, params Parameters, view *view.ForFormat) *Show {
	flags := flag.NewFlagSet("", flag.ContinueOnError)
	view.DefineFormatFlag(flags)
	flags.String("embed", "", "comma-separated list of relations to embed")
	flags.String("template", "", "template to use for display")
	flags.String(
		resource.Suffix("uuid"),
		"",
		fmt.Sprintf("id of the %s to display", resource.Name()),
	)

	args := config.NewArguments(resource.Prefix("show"), flags, []string{})

	return &Show{
		backend: backend,
		params: config.NewFirstMatch(
			args,
			config.NewPrefix(resource.Prefix("show"), params),
		),
		resource: resource,
		args:     args,
		view:     view,
	}
}

func (self *Show) Run(args []string) int {
	self.args.Parse(args)

	if tmpl := self.params.Get("template"); tmpl != "" {
		self.view.Add("tmpl", view.NewTemplate(tmpl, os.Stdout))
		self.view.Choose("tmpl")
	} else {
		self.view.Choose(self.params.Get("format"))
	}

	url := self.url()
	ix := interactions.NewShow(self.backend, url)
	interactions.Embed(ix, self.params.Get("embed"))
	result := ix.Run()
	if result.Error != nil {
		self.view.Error(result.Error)
		return 1
	}

	self.view.Display(result)
	return 0
}

func (self *Show) Name() string { return self.resource.Prefix("show") }
func (self *Show) Help() string {
	commandName := self.Name()

	return fmt.Sprintf(`%s: display a single %s

%s`, commandName, self.resource.Name(), self.args.Help())
}

func (self *Show) Synopsis() string {
	return fmt.Sprintf("show %s in detail", self.resource.Name())
}

func (self *Show) url() string {
	uuid := self.params.Get(self.resource.Param("uuid"))
	return self.resource.PathTo(uuid)
}
