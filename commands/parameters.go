package commands

type Parameters interface {
	Get(name string) string
	IsSet(name string) bool
}

type StaticParameters map[string]string

func (self StaticParameters) Get(name string) string {
	return self[name]
}

func (self StaticParameters) IsSet(name string) bool {
	_, found := self[name]
	return found
}
