package commands

import (
	"bytes"
	"fmt"
)

type OutputToBuffer struct {
	InfoTo  *bytes.Buffer
	ErrorTo *bytes.Buffer
}

func NewOutputToBuffer() *OutputToBuffer {
	return &OutputToBuffer{
		InfoTo:  bytes.NewBufferString(""),
		ErrorTo: bytes.NewBufferString(""),
	}
}

func (self *OutputToBuffer) Error(msg string) { fmt.Fprintf(self.ErrorTo, "%s\n", msg) }
func (self *OutputToBuffer) Info(msg string)  { fmt.Fprintf(self.InfoTo, "%s\n", msg) }
