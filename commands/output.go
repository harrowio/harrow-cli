package commands

type Output interface {
	Error(message string)
	Info(message string)
}
