package commands

import (
	"flag"
	"fmt"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
	"bitbucket.org/harrowio/harrow-cli/interactions"
	"bitbucket.org/harrowio/harrow-cli/model"
)

type CreateProject struct {
	backend backend.Interface
	params  Parameters
	args    *config.Arguments
	output  Output
}

func NewCreateProject(backend backend.Interface, params Parameters, input config.Asker, output Output) *CreateProject {
	flags := flag.NewFlagSet("", flag.ContinueOnError)
	flags.String("name", "", "name of the project")
	flags.Bool("public", true, "project will be publicly visible")
	flags.String("organization-uuid", "", "organization in which to create this project")

	args := config.NewArguments("create-project", flags, []string{})

	fromUser := config.NewFromUser(input, output)
	fromUser.Only("name")

	return &CreateProject{
		backend: backend,
		params: config.NewFirstMatch(
			args,
			config.NewPrefix("create-project", params),
			fromUser,
		),
		args:   args,
		output: output,
	}
}

func (self *CreateProject) Exec() (*model.Project, error) {
	args := &interactions.CreateProjectArguments{
		Name:             self.params.Get("name"),
		OrganizationUuid: self.params.Get("organization.uuid"),
	}
	if public := self.params.Get("public"); public != "" && public != "false" {
		args.Public = true
	}

	ix := interactions.NewCreateProject(self.backend)
	res := ix.Run(args)
	if res.Error != nil {
		return nil, res.Error
	}

	project, err := model.NewProject(res.Subject)
	if err != nil {
		return nil, err
	}

	return project, nil
}

func (self *CreateProject) Run(args []string) int {
	self.args.Parse(args)
	project, err := self.Exec()
	if err != nil {
		self.output.Error(err.Error())
		return 1
	}

	self.output.Info("Project-Id: " + project.Uuid)

	return 0
}

func (self *CreateProject) Help() string {
	return fmt.Sprintf(`create-project

Creates a new project on Harrow.

Newly created projects default to being publicly visible.  Specify --public to change this.

If --name is not provided, the user is asked to provide a name.

%s`, self.args.Help())
}

func (self *CreateProject) Synopsis() string {
	return `create a new project`
}
