package commands

import (
	"fmt"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
)

func TestListOrganizations_returnsUsersOrganizations(t *testing.T) {
	cfg := config.NewConfig()
	userUuid := "7c788593-1919-4243-acac-7e65360a2344"
	cfg.Set("user.uuid", userUuid)

	url := fmt.Sprintf("/users/%s/organizations", userUuid)
	be := backend.NewMock()
	be.RespondTo("GET", url, &backend.Response{
		Collection: []map[string]interface{}{
			{"subject": map[string]interface{}{
				"uuid": "3a6e4c68-59aa-472e-a6be-8ae9aac1d986",
				"name": "Organization A",
			},
			},
			{"subject": map[string]interface{}{
				"uuid": "179fe6e1-82ea-48e1-8932-2590194cda26",
				"name": "Organization B",
			},
			},
		},
		Error: nil,
	})

	cmd := NewListOrganizations(be, cfg, NewOutputToBuffer())
	organizations, err := cmd.Exec()
	if err != nil {
		t.Fatal(err)
	}

	if got, want := len(organizations), 2; got != want {
		t.Errorf("len(organizations) = %d; want %d", got, want)
	}
}

func TestListOrganizations_returnsProjectsForEachOrganization(t *testing.T) {
	cfg := config.NewConfig()
	userUuid := "7c788593-1919-4243-acac-7e65360a2344"
	organizationUuid := "3a6e4c68-59aa-472e-a6be-8ae9aac1d986"
	cfg.Set("user.uuid", userUuid)

	url := fmt.Sprintf("/users/%s/organizations", userUuid)
	projectsUrl := fmt.Sprintf("/organizations/%s/projects", organizationUuid)

	be := backend.NewMock()
	be.RespondTo("GET", url, &backend.Response{
		Collection: []map[string]interface{}{
			{"subject": map[string]interface{}{
				"uuid": organizationUuid,
				"name": "Organization A",
			},
			},
		},
		Error: nil,
	})
	be.RespondTo("GET", projectsUrl, &backend.Response{
		Collection: []map[string]interface{}{
			{
				"subject": map[string]interface{}{
					"uuid": "81abe042-a957-4afc-a174-9147822292f0",
					"name": "Project A",
				},
			},
		},
		Error: nil,
	})

	cmd := NewListOrganizations(be, cfg, NewOutputToBuffer())
	organizations, err := cmd.Exec()
	if err != nil {
		t.Fatal(err)
	}
	if got, want := len(organizations), 1; got != want {
		t.Errorf("len(organizations) = %d; want %d", got, want)
	}

	organization := organizations[0]

	if got, want := len(organization.Projects), 1; got != want {
		t.Errorf("len(organization.Projects) = %d; want %d", got, want)
	}
}
