package commands

import (
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
)

func TestCreateProject_returnsAProject(t *testing.T) {
	be := backend.NewMock()
	project := map[string]interface{}{
		"uuid":             "5d3c1f57-18d4-4c4a-827a-878826bf1959",
		"organizationUuid": "233fa5c3-5989-4080-9fee-fcc78daf2d3e",
		"name":             "Project",
	}

	be.RespondTo("POST", "/projects", &backend.Response{
		Subject: project,
	})

	params := StaticParameters{
		"public":            "true",
		"organization.uuid": "233fa5c3-5989-4080-9fee-fcc78daf2d3e",
	}

	input := config.Responses{
		"Name:": "Project",
	}

	output := NewOutputToBuffer()
	cmd := NewCreateProject(be, params, input, output)
	createdProject, err := cmd.Exec()
	if err != nil {
		t.Fatal(err)
	}

	if got, want := createdProject.Uuid, project["uuid"].(string); got != want {
		t.Errorf("createdProject.Uuid = %q; want %q", got, want)
	}
}
