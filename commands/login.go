package commands

import (
	"flag"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
	"bitbucket.org/harrowio/harrow-cli/interactions"
	"bitbucket.org/harrowio/harrow-cli/model"
)

type Login struct {
	backend backend.Interface
	params  Parameters
	config  *config.Config
	args    *config.Arguments
	output  Output
}

func NewLogin(backend backend.Interface, params Parameters, cfg *config.Config, output Output) *Login {
	flags := flag.NewFlagSet("login", flag.ContinueOnError)
	flags.String("email", "", "Username for login")
	args := config.NewArguments("login", flags, []string{})

	return &Login{
		backend: backend,
		params:  config.NewFirstMatch(args, config.NewPrefix("login", params)),
		config:  cfg,
		args:    args,
		output:  output,
	}
}

func (self *Login) Exec() (*model.Session, error) {
	ix := interactions.NewCreateSession(self.backend)
	res := ix.Run(&interactions.CreateSessionArguments{
		Email:    self.params.Get("email"),
		Password: self.params.Get("password"),
	})

	if res.Error != nil {
		return nil, res.Error
	}

	session, err := model.NewSession(res.Subject)
	if err == nil {
		self.config.Set("session.uuid", session.Uuid)
		self.config.Set("user.uuid", session.UserUuid)
	} else {
		return nil, err
	}

	local, err := config.Local()
	if err != nil {
		return nil, err
	}
	defer local.Close()

	if err := config.Save(self.config, local); err != nil {
		return nil, err
	}

	return session, nil
}

func (self *Login) Run(args []string) int {
	self.args.Parse(args)

	_, err := self.Exec()
	if err != nil {
		self.output.Error(err.Error())
		return 1
	} else {
		self.output.Info("Logged in.")
		return 0
	}
}

func (self *Login) Help() string {
	return `login [--email=EMAIL]

Logs into harrow, using EMAIL as the username.

This command prompts for the password interactively.  After a successful
login, the current session id is stored in the local configuration file.
`
}

func (self *Login) Synopsis() string { return `Log into Harrow` }
