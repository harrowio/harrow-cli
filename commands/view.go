package commands

type View interface {
	Display(data interface{})
	Error(err error)
}
