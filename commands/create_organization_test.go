package commands

import (
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/config"
)

func TestCreateOrganization_returnsAnOrganization(t *testing.T) {
	be := backend.NewMock()
	organization := map[string]interface{}{
		"uuid":   "5d3c1f57-18d4-4c4a-827a-878826bf1959",
		"name":   "Organization",
		"public": true,
	}

	be.RespondTo("PUT", "/organizations", &backend.Response{
		Subject: organization,
	})

	params := StaticParameters{
		"public": "true",
	}

	input := config.Responses{
		"Name:": "Organization",
	}

	output := NewOutputToBuffer()
	cmd := NewCreateOrganization(be, params, input, output)
	org, err := cmd.Exec()
	if err != nil {
		t.Fatal(err)
	}

	if got, want := org.Uuid, organization["uuid"].(string); got != want {
		t.Errorf("org.Uuid = %q; want %q", got, want)
	}
}
