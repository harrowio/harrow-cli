#!/bin/sh -eu

git_rev=$(git rev-parse --short HEAD)
cat <<EOF > const.go
package main

const (
	VERSION    = "$git_rev"
	USER_AGENT = "harrow-cli $git_rev"
)
EOF
gofmt -w const.go
