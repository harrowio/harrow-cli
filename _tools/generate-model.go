package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

const t = `package model

type {{.Type}} struct {
	Uuid       string
	Attributes map[string]interface{}
}

func New{{.Type}}(subject map[string]interface{}) (*{{.Type}}, error) {
	{{.Self}} := &{{.Type}}{
		Attributes: subject,
	}

	if err := {{.Self}}.Bind(subject); err != nil {
		return nil, err
	} else {
		return {{.Self}}, nil
	}
}

func (self *{{.Type}}) Bind(value interface{}) error {
	data, ok := value.(map[string]interface{})
	if !ok {
		return NewBindError("{{.Self}}", self, value)
	}

	if uuid, ok := data["uuid"].(string); ok {
		self.Uuid = uuid
	} else {
		return NewBindError("{{.Self}}.Uuid", self.Uuid, data["uuid"])
	}

	return nil
}
`

var (
	tmpl = template.Must(template.New("main").Parse(t))
)

type TemplateData struct {
	Type string
	Self string
}

func NewTemplateData(name string) *TemplateData {
	normalized := strings.ToLower(name)
	capitalized := strings.ToUpper(normalized[0:1]) + normalized[1:]
	return &TemplateData{
		Type: capitalized,
		Self: normalized,
	}
}

func writeModel(modelName string) {
	fname := filepath.Join("model", modelName+".go")
	_, err := os.Stat(fname)
	if err != nil && !os.IsNotExist(err) {
		log.Printf("writeModel: %s", err)
		return
	}

	if err == nil {
		log.Printf("ignore %s", fname)
		return
	}

	out, err := os.Create(fname)
	if err != nil {
		log.Printf("writeModel: %s", err)
	}
	defer out.Close()
	tmpl.Execute(out, NewTemplateData(modelName))
}

func main() {
	if len(os.Args) == 1 {
		usage()
	}

	for _, model := range os.Args[1:] {
		writeModel(model)
	}
}

func usage() {
	fmt.Fprintf(os.Stderr, `%s NAME...

Generate scaffolding for a model in models/NAME.go.

Fails if models/NAME.go already exists.
`, os.Args[0])
	os.Exit(1)
}
