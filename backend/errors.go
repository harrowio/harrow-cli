package backend

import "fmt"

type NotFoundError struct {
	Method string
	Path   string
}

func NewNotFoundError(method, path string) *NotFoundError {
	return &NotFoundError{Method: method, Path: path}
}

func (self *NotFoundError) Error() string {
	return fmt.Sprintf("backend: not found: %s %s", self.Method, self.Path)
}
