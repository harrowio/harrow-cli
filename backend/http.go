package backend

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Http struct {
	header http.Header
	client *http.Client
	url    string
}

type HALBody struct {
	Subject    interface{} `json:"subject,omitempty"`
	Collection interface{} `json:"collection,omitempty"`
	Links      interface{} `json:"_links,omitempty"`
}

func NewHttp(url string) *Http {
	return &Http{
		client: &http.Client{},
		url:    url,
		header: http.Header{},
	}
}

func (self *Http) SetHeader(key, value string) {
	self.header.Set(key, value)
}

func (self *Http) Get(path string) *Response {
	req, err := self.request("GET", path, nil)
	if err != nil {
		return &Response{Error: err}
	}

	return self.performRequest(req)
}

func (self *Http) Post(path string, params interface{}) *Response {
	req, err := self.request("POST", path, &HALBody{Subject: params})
	if err != nil {
		return &Response{Error: err}
	}

	return self.performRequest(req)
}

func (self *Http) Put(path string, params interface{}) *Response {
	req, err := self.request("PUT", path, &HALBody{Subject: params})
	if err != nil {
		return &Response{Error: err}
	}

	return self.performRequest(req)
}

func (self *Http) request(method, path string, params interface{}) (*http.Request, error) {
	req, err := (*http.Request)(nil), (error)(nil)
	url := path
	if path[0] == '/' {
		url = self.url + path
	}

	if params != nil {
		serialized, err := json.MarshalIndent(params, "", "  ")
		if err != nil {
			return nil, err
		}
		buf := bytes.NewBuffer(serialized)

		req, err = http.NewRequest(method, url, buf)
	} else {
		req, err = http.NewRequest(method, url, nil)
	}

	if err != nil {
		return nil, err
	}

	self.addHeadersTo(req)
	return req, nil
}

func (self *Http) addHeadersTo(req *http.Request) {
	for key, value := range self.header {
		req.Header[key] = value
	}
}

func (self *Http) performRequest(req *http.Request) *Response {
	res, err := self.client.Do(req)
	if err != nil {
		return &Response{Error: err}
	}
	defer res.Body.Close()

	response := Response{}
	unwrap := HALBody{Subject: &response.Subject, Collection: &response.Collection, Links: &response.Links}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return &Response{
			Error: &HttpError{
				Inner:    err,
				Response: res,
				Body:     body,
			},
		}
	}

	if res.StatusCode >= 500 {
		return &Response{
			Error: &HttpError{
				Inner:    errors.New(http.StatusText(res.StatusCode)),
				Response: res,
				Body:     body,
			},
		}
	}

	if err := json.Unmarshal(body, &unwrap); err != nil {
		if res.StatusCode == http.StatusNoContent {
			return &response
		}
		return &Response{
			Error: &HttpError{
				Inner:    err,
				Response: res,
				Body:     body,
			},
		}
	}

	return &response
}

type HttpError struct {
	Inner    error
	Response *http.Response
	Body     []byte
}

func (self *HttpError) Error() string {
	return fmt.Sprintf("%d %s: %s\n---\n%s\n",
		self.Response.StatusCode,
		http.StatusText(self.Response.StatusCode),
		self.Inner,
		self.Body,
	)
}
