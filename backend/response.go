package backend

type Response struct {
	Subject    map[string]interface{}
	Collection []map[string]interface{}
	Links      Links
	Error      error
}

type Links map[string]Link

func (self Links) Rel(name string) string {
	return self[name].Href()
}

type Link map[string]string

func (self Link) Href() string {
	return self["href"]
}
