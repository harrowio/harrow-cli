package backend

type Interface interface {
	SetHeader(key, value string)
	Post(path string, params interface{}) *Response
	Put(path string, params interface{}) *Response
	Get(path string) *Response
}
