package backend

import "fmt"

type Mock struct {
	responses map[string]*Response
	header    map[string]string
}

func NewMock() *Mock {
	return &Mock{
		header:    map[string]string{},
		responses: map[string]*Response{},
	}
}

func (self *Mock) SetHeader(key, value string) {
	self.header[key] = value
}

func (self *Mock) Get(path string) *Response {
	return self.response("GET", path)
}

func (self *Mock) Put(path string, params interface{}) *Response {
	return self.response("PUT", path)
}

func (self *Mock) Post(path string, params interface{}) *Response {
	return self.response("POST", path)
}

func (self *Mock) RespondTo(method, path string, response *Response) {
	key := fmt.Sprintf("%s %s", method, path)
	self.responses[key] = response
}

func (self *Mock) response(method, path string) *Response {
	key := fmt.Sprintf("%s %s", method, path)
	response, found := self.responses[key]

	if found {
		return response
	}

	return &Response{
		Error: NewNotFoundError(method, path),
	}
}
