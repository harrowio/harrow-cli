package backend

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/model"
)

func TestHttp_Get_makesHTTPRequest(t *testing.T) {
	mux := http.NewServeMux()
	requests := 0
	path := "/users/63d0a004-4e52-4a74-89c5-d67454272b0c/organizations"
	mux.HandleFunc(path, func(w http.ResponseWriter, req *http.Request) {
		if req.Method == "GET" {
			requests++
		}
		w.WriteHeader(http.StatusNoContent)
	})
	server := httptest.NewServer(mux)
	be := NewHttp(server.URL)
	res := be.Get(path)
	if res.Error != nil {
		t.Error(res.Error)
	}

	if got, want := requests, 1; got != want {
		t.Errorf("requests = %d; want %d", got, want)
	}
}

func TestHttp_Post_makesHTTPRequest(t *testing.T) {
	mux := http.NewServeMux()
	requests := 0
	mux.HandleFunc("/sessions", func(w http.ResponseWriter, req *http.Request) {
		if req.Method == "POST" {
			requests++
		}
		w.WriteHeader(http.StatusNoContent)
	})
	server := httptest.NewServer(mux)
	be := NewHttp(server.URL)
	res := be.Post("/sessions", &model.Session{})
	if res.Error != nil {
		t.Error(res.Error)
	}

	if got, want := requests, 1; got != want {
		t.Errorf("requests = %d; want %d", got, want)
	}
}

func TestHttp_Post_wrapsParamWithSubject(t *testing.T) {
	mux := http.NewServeMux()
	result := struct{ Subject map[string]interface{} }{}

	mux.HandleFunc("/sessions", func(w http.ResponseWriter, req *http.Request) {
		dec := json.NewDecoder(req.Body)
		if err := dec.Decode(&result); err != nil {
			t.Fatal(err)
		}
		w.WriteHeader(http.StatusNoContent)
	})
	server := httptest.NewServer(mux)
	be := NewHttp(server.URL)
	subject := map[string]interface{}{
		"uuid": "3b46f3e8-9167-4e5d-b621-8cd5b77d7f26",
	}

	res := be.Post("/sessions", subject)
	if res.Error != nil {
		t.Error(res.Error)
	}

	if got, want := result.Subject["uuid"], subject["uuid"]; got != want {
		t.Errorf("result.Subject[%q] = %q; want %q", "uuid", got, want)
	}
}

func TestHttp_Post_unwrapsSubjectFromResponse(t *testing.T) {
	mux := http.NewServeMux()

	subject := map[string]interface{}{
		"uuid": "3b46f3e8-9167-4e5d-b621-8cd5b77d7f26",
	}
	mux.HandleFunc("/sessions", func(w http.ResponseWriter, req *http.Request) {
		enc := json.NewEncoder(w)
		if err := enc.Encode(&HALBody{Subject: subject}); err != nil {
			t.Fatal(err)
		}
	})
	server := httptest.NewServer(mux)
	be := NewHttp(server.URL)

	res := be.Post("/sessions", subject)
	if res.Error != nil {
		t.Error(res.Error)
	}

	if got, want := res.Subject["uuid"], subject["uuid"]; got != want {
		t.Errorf("result.Subject[%q] = %q; want %q", "uuid", got, want)
	}
}

func TestHttp_AllowsHeadersToBeSet(t *testing.T) {
	mux := http.NewServeMux()

	got := ""
	mux.HandleFunc("/sessions", func(w http.ResponseWriter, req *http.Request) {
		got = req.Header.Get("X-Harrow-Session-Uuid")
		w.WriteHeader(http.StatusNoContent)
	})
	server := httptest.NewServer(mux)
	be := NewHttp(server.URL)
	want := "22b3327e-c64a-43bf-bd1f-fab19e557d45"
	be.SetHeader("X-Harrow-Session-Uuid", want)

	res := be.Post("/sessions", nil)
	if res.Error != nil {
		t.Error(res.Error)
	}

	if got != want {
		t.Errorf("req.Header.Get(%q) = %q; want %q", "X-Harrow-Session-Uuid", got, want)
	}
}
