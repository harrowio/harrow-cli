package config

import "testing"

func TestFirstMatch_returnsTheFirstMatch(t *testing.T) {
	a := NewConfig()
	a.Set("a", "a")

	b := NewConfig()
	b.Set("b", "b")

	c := NewConfig()
	c.Set("b", "c")

	cfg := NewFirstMatch(
		a,
		b,
		c,
	)

	if got, want := cfg.Get("b"), "b"; got != want {
		t.Errorf("cfg.Get(%q) = %q; want %q", "b", got, want)
	}
}
