package config

import (
	"fmt"
	"sort"
	"strings"
)

type FromUser struct {
	input     Asker
	output    Output
	whitelist sort.StringSlice
}

func NewFromUser(input Asker, output Output) *FromUser {
	return &FromUser{input: input, output: output}
}

func (self *FromUser) IsSet(key string) bool { return false }

func (self *FromUser) Get(key string) string {
	if !self.has(key) {
		return ""
	}
	value, err := "", (error)(nil)
	if key == "password" {
		value, err = self.input.AskSecret(
			self.formatField(key),
		)
		self.output.Info("\n")
	} else {
		value, err = self.input.Ask(
			self.formatField(key),
		)
	}

	if err != nil {
		self.output.Error(err.Error())
	}

	return value
}

func (self *FromUser) formatField(field string) string {
	return fmt.Sprintf("%s%s:", strings.ToUpper(field[0:1]), field[1:])
}

func (self *FromUser) Only(allowed ...string) {
	self.whitelist = append(self.whitelist, allowed...)
	self.whitelist.Sort()
}

func (self *FromUser) has(param string) bool {
	if self.whitelist == nil {
		return true
	}

	x := self.whitelist.Search(param)
	return x != len(self.whitelist) && self.whitelist[x] == param
}
