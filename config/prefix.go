package config

import "fmt"

type Prefix struct {
	prefix string
	next   Getter
}

func NewPrefix(prefix string, next Getter) *Prefix {
	return &Prefix{prefix: prefix, next: next}
}

func (self *Prefix) Get(key string) string {
	keyWithPrefix := fmt.Sprintf("%s.%s", self.prefix, key)
	if self.next.IsSet(keyWithPrefix) {
		return self.next.Get(keyWithPrefix)
	}

	return self.next.Get(key)
}

func (self *Prefix) IsSet(key string) bool {
	keyWithPrefix := fmt.Sprintf("%s.%s", self.prefix, key)
	if self.next.IsSet(keyWithPrefix) {
		return true
	}

	return self.next.IsSet(key)
}
