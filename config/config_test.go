package config

import (
	"bytes"
	"testing"
)

type BufferConfigStore struct {
	*bytes.Buffer
}

func NewBufferConfigStore() *BufferConfigStore {
	return &BufferConfigStore{
		Buffer: new(bytes.Buffer),
	}
}

func (self *BufferConfigStore) Truncate(size int64) error {
	self.Buffer.Reset()
	return nil
}

func (self *BufferConfigStore) Close() error {
	return nil
}

func TestConfig_Get(t *testing.T) {
	cfg := NewConfig()
	cfg.Set("session.uuid", "2cfabebc-6b71-41d5-b595-e6de86b2b0b1")

	if got, want := cfg.Get("session.uuid"), "2cfabebc-6b71-41d5-b595-e6de86b2b0b1"; got != want {
		t.Errorf("cfg.Get(%q) = %q; want %q", "session.uuid", got, want)
	}

}

func TestSave_storesConfig(t *testing.T) {
	cfg := NewConfig()
	cfg.Set("session.uuid", "2cfabebc-6b71-41d5-b595-e6de86b2b0b1")
	out := NewBufferConfigStore()

	if err := Save(cfg, out); err != nil {
		t.Fatal(err)
	}

	if got := len(out.Bytes()); got == 0 {
		t.Errorf("len(out) = %d; want not 0", got)
	}
}

func TestLoad_restoresConfig(t *testing.T) {
	cfg := NewConfig()
	cfg.Set("session.uuid", "2cfabebc-6b71-41d5-b595-e6de86b2b0b1")
	out := NewBufferConfigStore()

	if err := Save(cfg, out); err != nil {
		t.Fatal(err)
	}

	loaded := NewConfig()
	if err := Load(loaded, bytes.NewReader(out.Bytes())); err != nil {
		t.Fatal(err)
	}

	if got, want := loaded.Get("session.uuid"), cfg.Get("session.uuid"); got != want {
		t.Errorf("loaded.Get(%q) = %q; want %q", "session.uuid", got, want)
	}
}
