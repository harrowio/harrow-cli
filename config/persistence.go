package config

import (
	"encoding"
	"fmt"
	"io"
	"io/ioutil"
)

func Save(data encoding.TextMarshaler, to ConfigStore) error {
	raw, err := data.MarshalText()
	if err != nil {
		return err
	}

	if err := to.Truncate(0); err != nil {
		return err
	}

	_, err = fmt.Fprintf(to, "%s\n", raw)

	return err
}

func Load(dest encoding.TextUnmarshaler, from io.Reader) error {
	data, err := ioutil.ReadAll(from)
	if err != nil {
		return err
	}

	if len(data) == 0 {
		return nil
	}

	return dest.UnmarshalText(data)
}
