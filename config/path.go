package config

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

var (
	ErrNoHome = fmt.Errorf("config: HOME is unset")

	Local  = LocalConfigFile
	Global = GlobalConfigFile
)

type ConfigStore interface {
	io.ReadWriteCloser
	Truncate(size int64) error
}

func TempConfigFile(tag string) func() (ConfigStore, error) {
	return func() (ConfigStore, error) {
		return os.OpenFile(
			filepath.Join(os.TempDir(), "harrowrc-"+tag),
			os.O_RDWR|os.O_CREATE,
			0600,
		)
	}
}

func LocalConfigFile() (ConfigStore, error) {
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	return os.OpenFile(filepath.Join(cwd, ".harrowrc"), os.O_RDWR|os.O_CREATE, 0600)
}

func GlobalConfigFile() (ConfigStore, error) {
	cwd := os.Getenv("HOME")

	if cwd == "" {
		return nil, ErrNoHome
	}

	return os.OpenFile(filepath.Join(cwd, ".harrowrc"), os.O_RDWR|os.O_CREATE, 0600)

}
