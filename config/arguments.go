package config

import (
	"bytes"
	"flag"
	"fmt"
	"reflect"
	"strings"
)

type Arguments struct {
	name           string
	flagsPrototype *flag.FlagSet
	flags          *flag.FlagSet
	vars           map[string]interface{}
}

func NewArguments(name string, flags *flag.FlagSet, args []string) *Arguments {
	result := &Arguments{
		name:           name,
		flagsPrototype: flags,
		flags:          flag.NewFlagSet(name, flag.ContinueOnError),
		vars:           map[string]interface{}{},
	}

	result.Parse(args)

	return result
}

func (self *Arguments) cloneFlags() {
	self.vars = map[string]interface{}{}
	self.flags = flag.NewFlagSet(self.name, flag.ExitOnError)
	self.flagsPrototype.VisitAll(self.addFlag)
}

func (self *Arguments) addFlag(flag *flag.Flag) {
	name := self.varName(flag.Name)
	if flag.DefValue == "true" || flag.DefValue == "false" {
		self.vars[name] = self.flags.Bool(flag.Name, flag.DefValue == "true", flag.Usage)
	} else {
		self.vars[name] = self.flags.String(flag.Name, flag.DefValue, flag.Usage)
	}
}

func (self *Arguments) Help() string {
	out := new(bytes.Buffer)
	fmt.Fprintf(out, "Flags:\n")
	printFlag := func(flag *flag.Flag) {
		fmt.Fprintf(out, "\n  --%s=%#v\n    %s\n", flag.Name, flag.DefValue, flag.Usage)
	}
	self.flagsPrototype.VisitAll(printFlag)
	return out.String()
}

func (self *Arguments) Parse(args []string) {
	self.cloneFlags()
	self.flags.Parse(args)
}

func (self *Arguments) NArg() int {
	return self.flags.NArg()
}

func (self *Arguments) Args() []string {
	return self.flags.Args()
}

func (self *Arguments) Arg(i int) string {
	return self.flags.Arg(i)
}

func (self *Arguments) Get(key string) string {
	flagvar, found := self.vars[key]
	if found {
		return fmt.Sprintf("%v", reflect.ValueOf(flagvar).Elem().Interface())
	}

	return ""
}

func (self *Arguments) varName(flagName string) string {
	return strings.Replace(strings.ToLower(flagName), "-", ".", -1)
}

func (self *Arguments) IsSet(key string) bool {
	flag := self.flags.Lookup(key)
	if flag == nil {
		return false
	}

	flagvar := self.vars[key]
	if flagvar == nil {
		return false
	}

	return fmt.Sprintf("%v", flagvar) != flag.DefValue
}
