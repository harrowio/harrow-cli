package config

import "encoding/json"

type Config struct {
	settings map[string]string
}

func NewConfig() *Config {
	return &Config{
		settings: map[string]string{},
	}
}

func (self *Config) Set(key, value string) {
	self.settings[key] = value
}

func (self *Config) Get(key string) string {
	return self.settings[key]
}

func (self *Config) IsSet(key string) bool {
	_, found := self.settings[key]
	return found
}

func (self *Config) Unset(key string) {
	delete(self.settings, key)
}

func (self *Config) MarshalText() ([]byte, error) {
	result, err := json.MarshalIndent(self.settings, "", "  ")

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (self *Config) UnmarshalText(text []byte) error {
	return json.Unmarshal(text, &self.settings)
}
