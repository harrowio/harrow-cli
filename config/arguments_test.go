package config

import (
	"flag"
	"testing"
)

func TestArguments_Get_looksUpFlagByName(t *testing.T) {
	cmdline := flag.NewFlagSet("harrow-cli", flag.ContinueOnError)
	cmdline.String("session-uuid", "not-logged-in", "your current session")

	uuid := "5539de00-42a0-443e-a35b-d2428ff30a77"
	args := []string{"--session-uuid", uuid, "rest"}

	cfg := NewArguments("test", cmdline, args)

	if got, want := cfg.Get("session.uuid"), uuid; got != want {
		t.Errorf("cfg.Get(%q) = %q; want %q", "session.uuid", got, want)
	}
}

func TestArguments_IsSet_returnsTrueIfFlagIsNotTheDefaultValue(t *testing.T) {
	cmdline := flag.NewFlagSet("harrow-cli", flag.ContinueOnError)
	cmdline.String("global", "false", "write to global configuration")
	args := []string{"--global", "true"}

	cfg := NewArguments("test", cmdline, args)

	if got, want := cfg.IsSet("global"), true; got != want {
		t.Errorf("cfg.IsSet(%q) = %v; want %v", "global", got, want)
	}
}

func TestArguments_ItSupportsBooleanFlags(t *testing.T) {
	cmdline := flag.NewFlagSet("harrow-cli", flag.ContinueOnError)
	cmdline.Bool("global", false, "write to global configuration")
	args := []string{"--global", "arg"}

	cfg := NewArguments("test", cmdline, args)

	if got, want := cfg.Get("global"), "true"; got != want {
		t.Errorf("cfg.Get(%q) = %v; want %v", "global", got, want)
	}
}
