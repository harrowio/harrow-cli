package config

type Asker interface {
	Ask(query string) (string, error)
	AskSecret(query string) (string, error)
}

type Responses map[string]string

func (self Responses) Ask(query string) (string, error) {
	return self[query], nil
}

func (self Responses) AskSecret(query string) (string, error) {
	return self[query], nil
}

type Output interface {
	Error(message string)
	Info(message string)
}
