package config

import (
	"bytes"
	"fmt"
	"os"
	"strings"
)

type Environment struct {
	prefix string
}

func NewEnvironment(prefix string) *Environment {
	return &Environment{
		prefix: prefix,
	}
}

func (self *Environment) Get(key string) string {
	varname := self.Var(key)
	return os.Getenv(varname)
}

func (self *Environment) IsSet(key string) bool {
	return self.Get(key) != ""
}

func (self *Environment) Var(name string) string {
	varname := bytes.NewBufferString(self.prefix)
	fields := strings.Split(name, ".")
	for _, field := range fields {
		fmt.Fprintf(varname, "_%s", strings.ToUpper(field))
	}

	return varname.String()
}
