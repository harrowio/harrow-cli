package config

import (
	"os"
	"testing"
)

func TestEnvironment_Get_readsValueFromEnvironment(t *testing.T) {
	cfg := NewEnvironment("HRW")
	uuid := "b1e077a2-bb69-44da-8320-871351e57faf"
	os.Setenv("HRW_SESSION_UUID", uuid)

	if got, want := cfg.Get("session.uuid"), uuid; got != want {
		t.Fatalf("cfg.Get(%q) = %q; want %q", "session.uuid", got, want)
	}
}
