package config

import (
	"fmt"
	"io"
)

type ErrorPrinter struct {
	w io.Writer
}

func NewErrorPrinter(to io.Writer) *ErrorPrinter {
	return &ErrorPrinter{w: to}
}

func (self *ErrorPrinter) Error(message string) {
	fmt.Fprintf(self.w, "error: %s\n", message)
}

func (self *ErrorPrinter) Info(message string) {}
