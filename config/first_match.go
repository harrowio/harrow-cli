package config

type Getter interface {
	Get(key string) string
	IsSet(key string) bool
}

type FirstMatch []Getter

func NewFirstMatch(sources ...Getter) FirstMatch {
	return FirstMatch(sources)
}

func (self FirstMatch) IsSet(name string) bool {
	for _, params := range self {
		if params.IsSet(name) {
			return true
		}
	}

	return false
}

func (self FirstMatch) Get(name string) string {
	for _, params := range self {
		value := params.Get(name)
		if value != "" {
			return value
		}
	}

	return ""
}
