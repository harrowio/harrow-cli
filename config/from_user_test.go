package config

import (
	"bytes"
	"testing"
)

func TestFromUser_asksForInput(t *testing.T) {
	responses := Responses{
		"Email:":    "vagrant@localhost",
		"Password:": "vagrant",
	}
	errbuf := bytes.NewBufferString("")
	errors := NewErrorPrinter(errbuf)

	cfg := NewFromUser(responses, errors)

	if got, want := cfg.Get("email"), "vagrant@localhost"; got != want {
		t.Errorf("cfg.Get(%q) = %q; want %q", "email", got, want)
	}
}

func TestFromUser_asksOnlyForWhitelistedFields_ifWhitelistProvided(t *testing.T) {
	responses := Responses{
		"Email:":    "vagrant@localhost",
		"Password:": "vagrant",
	}
	errbuf := bytes.NewBufferString("")
	errors := NewErrorPrinter(errbuf)

	cfg := NewFromUser(responses, errors)
	cfg.Only("password")

	if got, want := cfg.Get("email"), ""; got != want {
		t.Errorf("cfg.Get(%q) = %q; want %q", "email", got, want)
	}
}
