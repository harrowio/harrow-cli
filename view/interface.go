package view

type Interface interface {
	Display(data interface{})
	Error(err error)
}
