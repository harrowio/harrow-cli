package view

import (
	"flag"
	"fmt"
)

type ForFormat struct {
	byFormat  map[string]Interface
	current   Interface
	supported []string
}

func NewForFormat(format string, handler Interface) *ForFormat {
	return &ForFormat{
		byFormat: map[string]Interface{
			format: handler,
		},
		supported: []string{format},
		current:   handler,
	}
}

func (self *ForFormat) Choose(format string) {
	next, found := self.byFormat[format]
	if !found {
		self.current.Error(fmt.Errorf("unsupported format: %s", format))
		return
	}

	self.current = next
}

func (self *ForFormat) DefineFormatFlag(flags *flag.FlagSet) {
	flags.String(
		"format",
		"txt",
		fmt.Sprintf("output format (valid: %v)", self.supported),
	)
}

func (self *ForFormat) Add(format string, handler Interface) {
	if _, found := self.byFormat[format]; !found {
		self.supported = append(self.supported, format)
	}
	self.byFormat[format] = handler
}

func (self *ForFormat) Display(data interface{}) {
	self.current.Display(data)
}

func (self *ForFormat) Error(err error) {
	self.current.Error(err)
}
