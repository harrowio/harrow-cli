package view

import (
	"encoding/json"
	"fmt"
	"io"
)

type JSONView struct {
	out io.Writer
}

func NewJSON(w io.Writer) *JSONView {
	return &JSONView{out: w}
}

func (self *JSONView) Display(data interface{}) {
	output, _ := json.MarshalIndent(data, "", "  ")
	fmt.Fprintf(self.out, "%s\n", output)
}

func (self *JSONView) Error(err error) {
	wrapper := struct {
		Error error `json:"error"`
	}{err}

	if data, err := json.MarshalIndent(&wrapper, "", "  "); err != nil {
		fmt.Fprintf(self.out, "json.MarshalIndent: %s\n", err)
	} else {
		fmt.Fprintf(self.out, "%s\n", data)
	}
}
