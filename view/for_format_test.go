package view

import (
	"bytes"
	"testing"

	"github.com/mitchellh/cli"
)

func TestForFormat_callsDisplayForChosenFormat(t *testing.T) {
	ui := &cli.MockUi{
		InputReader: new(bytes.Buffer),
	}
	v := NewForFormat("json", NewJSON(new(bytes.Buffer)))
	v.Add("txt", NewText(ui))

	v.Choose("txt")
	v.Display("test")

	if got, want := ui.OutputWriter.String(), "test\n"; got != want {
		t.Errorf("ui.OutputWriter.String() = %q; want %q", got, want)
	}
}

func TestForFormat_callsDisplayForDefaultFormat_ifNoFormatChosen(t *testing.T) {
	out := new(bytes.Buffer)
	ui := &cli.MockUi{
		InputReader: new(bytes.Buffer),
	}
	v := NewForFormat("json", NewJSON(out))
	v.Add("txt", NewText(ui))

	v.Display("test")

	if got, want := out.String(), `"test"`+"\n"; got != want {
		t.Errorf("out.String() = %q; want %q", got, want)
	}
}
