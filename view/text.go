package view

import (
	"fmt"

	"github.com/mitchellh/cli"

	"bitbucket.org/harrowio/harrow-cli/interactions"
	"bitbucket.org/harrowio/harrow-cli/model"
)

type TextView struct {
	output cli.Ui
}

func NewText(output cli.Ui) *TextView {
	return &TextView{output: output}
}

func (self *TextView) displayHeading(level int, text string) {
	prefix := ""
	for i := 0; i < level; i++ {
		prefix = prefix + "#"
	}

	self.output.Info(prefix + " " + text)
}

func (self *TextView) Display(data interface{}) {
	switch dot := data.(type) {
	case *interactions.Result:
		self.displayHeading(1, "self")
		self.displayResult(dot)
	case map[string]interface{}:
		self.displayItem(dot)
	case []map[string]interface{}:
		self.displayCollection(dot)
	default:
		self.displayDump(dot)
	}
}

func (self *TextView) displayResult(data *interactions.Result) {
	if subject := data.Subject; subject != nil {
		self.displayItem(data.Subject)
	} else {
		self.displayCollection(data.Collection)
	}

	embedded := data.Embedded
	if embedded != nil {
		for rel, res := range embedded {
			self.displayHeading(2, rel)
			self.displayResult(res)
		}
	}
}

func (self *TextView) displayItem(data map[string]interface{}) {
	keys := model.SortedAttributeNames(data)

	line := fmt.Sprintf("%18s: %v", "uuid", data["uuid"])
	self.output.Info(line)
	for _, key := range keys {
		line := fmt.Sprintf("%18s: %v", key, data[key])
		self.output.Info(line)
	}
}

func (self *TextView) displayCollection(data []map[string]interface{}) {
	for _, item := range data {
		subject := item["subject"].(map[string]interface{})
		if name, ok := subject["name"].(string); ok {
			self.displayHeading(3, name)
		} else if uuid, ok := subject["uuid"].(string); ok {
			self.displayHeading(3, uuid)
		}

		self.displayItem(subject)
	}
}

func (self *TextView) displayDump(data interface{}) {
	self.output.Info(fmt.Sprintf("%v", data))
}

func (self *TextView) Error(err error) {
	self.output.Error(err.Error())
}
