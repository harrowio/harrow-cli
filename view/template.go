package view

import (
	"fmt"
	"io"
	"text/template"
)

type Template struct {
	to   io.Writer
	src  string
	tmpl *template.Template
	err  error
}

func NewTemplate(src string, to io.Writer) *Template {
	tmpl, err := template.New("main").Parse(src)
	return &Template{
		tmpl: tmpl,
		err:  err,
		to:   to,
		src:  src,
	}
}

func (self *Template) Display(data interface{}) {
	if self.err != nil {
		fmt.Fprintf(self.to, "error: %s in:\n%s\n", self.err, self.src)
		return
	}

	self.tmpl.Execute(self.to, data)
}

func (self *Template) Error(err error) {
	fmt.Fprintf(self.to, "error: %s\n", err)
}
