# Harrow Command Line Client

Command line client for https://www.harrow.io/

## Configuration

For a quick start, try this before using the alpha command line client:

```
$ export HARROW_HOST=https://www.vm.harrow.io/api
```

## Installation

### Binary / Package

Sorry there are no packages yet, we need to make time to package the CLI for
Homebrew, and the common Linuxes, at the very least, please excuse us!

### From Source

You'll need the Go language runtimes, but you can do the following:

```
  $ cd ~
  $ mkdir -p src/bitbucket.org/harrowio/
  $ git clone git@bitbucket.org:harrowio/harrow-cli.git src/bitbucket.org/harrowio/harrow-cli
  $ GOPATH=$(pwd) go get github.com/mitchellh/cli
  $ GOPATH=$(pwd) go install bitbucket.org/harrowio/harrow-cli
```

## Configuration

The following configuration settings are in use and can bet set with
`harrow-cli config`:

- `host`: the URL against which to make API requests.  Needs to be HTTPS.
  Example when running against the development virtual machine:

```
      harrow-cli config host https://www.vm.harrow.io/api
```

- `session.uuid`: set by the login command upon successful login.
   Identifies the current session.

- `user.uuid`: set by the login command upon successful login.
   Identifies the current user.  This user is used as a reference
   when plainly listing entities such as with the `list-organizations`
   subcommand.

## Licence

```
The MIT License (MIT)

Copyright (c) 2015 Harrow Unternehmergesellschaft (haftungsbeschränkt)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
