package model

import "fmt"

type BindError struct {
	Field string
	To    interface{}
	Value interface{}
}

func NewBindError(field string, dest, value interface{}) error {
	return &BindError{
		Field: field,
		To:    dest,
		Value: value,
	}
}

func (self *BindError) Error() string {
	return fmt.Sprintf("model: cannot bind %s(%T) from %T", self.Field, self.To, self.Value)
}
