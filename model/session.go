package model

type Session struct {
	Uuid       string
	UserUuid   string
	Valid      bool
	Attributes map[string]interface{}
}

func NewSession(attributes map[string]interface{}) (*Session, error) {
	session := &Session{
		Attributes: attributes,
	}

	return session, session.Bind(attributes)
}

func (self *Session) Bind(value interface{}) error {
	data, ok := value.(map[string]interface{})
	if !ok {
		return NewBindError("session", self, value)
	}

	if uuid, ok := data["uuid"].(string); ok {
		self.Uuid = uuid
	} else {
		return NewBindError("session.Uuid", self.Uuid, data["uuid"])
	}

	if valid, ok := data["valid"].(bool); ok {
		self.Valid = valid
	} else {
		return NewBindError("session.Valid", self.Valid, data["valid"])
	}

	if uuid, ok := data["userUuid"].(string); ok {
		self.UserUuid = uuid
	} else {
		return NewBindError("session.UserUuid", self.UserUuid, data["userUuid"])
	}

	return nil
}
