package model

import (
	"reflect"
	"testing"
)

func TestSession_Bind(t *testing.T) {
	session := &Session{}

	input := map[string]interface{}{
		"uuid":     "93b5c8a9-65ec-4773-ad02-309239902f2b",
		"userUuid": "5092f2dd-50af-4bd7-bdf6-7027b69bbf4d",
		"valid":    true,
	}

	if err := session.Bind(input); err != nil {
		t.Error(err)
	}

	if got, want := session.Uuid, input["uuid"]; got != want {
		t.Errorf("session.Uuid = %q; want %q", got, want)
	}

	if got, want := session.Valid, input["valid"]; got != want {
		t.Errorf("session.Valid = %#v; want %#v", got, want)
	}

	if got, want := session.UserUuid, input["userUuid"]; got != want {
		t.Errorf("session.UserUuid = %#v; want %#v", got, want)
	}
}

func TestNewSession_keepsAttributes(t *testing.T) {
	input := map[string]interface{}{
		"uuid":     "93b5c8a9-65ec-4773-ad02-309239902f2b",
		"userUuid": "5092f2dd-50af-4bd7-bdf6-7027b69bbf4d",
		"valid":    true,
		"canary":   "chirp",
	}

	session, err := NewSession(input)
	if err != nil {
		t.Fatal(err)
	}

	if got, want := session.Attributes, input; !reflect.DeepEqual(got, want) {
		t.Errorf("session.Attributes = %#v; want %#v", got, want)
	}
}

func TestNewSession_bindsAttributes(t *testing.T) {
	input := map[string]interface{}{
		"uuid":     "93b5c8a9-65ec-4773-ad02-309239902f2b",
		"userUuid": "5092f2dd-50af-4bd7-bdf6-7027b69bbf4d",
		"valid":    true,
		"canary":   "chirp",
	}

	session, err := NewSession(input)
	if err != nil {
		t.Fatal(err)
	}

	if got, want := session.Uuid, input["uuid"]; got != want {
		t.Errorf("session.Uuid = %q; want %q", got, want)
	}
}
