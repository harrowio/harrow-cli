package model

type Organization struct {
	Uuid       string
	Name       string
	Projects   []*Project
	Attributes map[string]interface{}
}

func NewOrganization(subject map[string]interface{}) (*Organization, error) {
	organization := &Organization{
		Attributes: subject,
		Projects:   []*Project{},
	}

	err := organization.Bind(subject)
	if err != nil {
		return nil, err
	} else {
		return organization, nil
	}
}

func (self *Organization) Bind(value interface{}) error {
	data, ok := value.(map[string]interface{})
	if !ok {
		return NewBindError("organization", self, value)
	}

	if uuid, ok := data["uuid"].(string); ok {
		self.Uuid = uuid
	} else {
		return NewBindError("organization.Uuid", self.Uuid, data["uuid"])
	}

	if name, ok := data["name"].(string); ok {
		self.Name = name
	} else {
		return NewBindError("organization.Name", self.Name, data["name"])
	}

	return nil
}
