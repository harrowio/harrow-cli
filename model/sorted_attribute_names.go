package model

import "sort"

func SortedAttributeNames(attributes map[string]interface{}) []string {
	keys := sort.StringSlice{}
	for key, _ := range attributes {
		if key != "uuid" {
			keys = append(keys, key)
		}
	}

	keys.Sort()

	return keys
}
