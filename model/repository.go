package model

type Repository struct {
	Uuid       string
	Attributes map[string]interface{}
}

func NewRepository(subject map[string]interface{}) (*Repository, error) {
	repository := &Repository{
		Attributes: subject,
	}

	if err := repository.Bind(subject); err != nil {
		return nil, err
	} else {
		return repository, nil
	}
}

func (self *Repository) Bind(value interface{}) error {
	data, ok := value.(map[string]interface{})
	if !ok {
		return NewBindError("repository", self, value)
	}

	if uuid, ok := data["uuid"].(string); ok {
		self.Uuid = uuid
	} else {
		return NewBindError("repository.Uuid", self.Uuid, data["uuid"])
	}

	return nil
}
