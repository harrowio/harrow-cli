package model

type Job struct {
	Uuid       string
	Name       string
	Attributes map[string]interface{}
}

func NewJob(subject map[string]interface{}) (*Job, error) {
	job := &Job{
		Attributes: subject,
	}

	if err := job.Bind(subject); err != nil {
		return nil, err
	} else {
		return job, nil
	}
}

func (self *Job) Bind(value interface{}) error {
	data, ok := value.(map[string]interface{})
	if !ok {
		return NewBindError("job", self, value)
	}

	if uuid, ok := data["uuid"].(string); ok {
		self.Uuid = uuid
	} else {
		return NewBindError("job.Uuid", self.Uuid, data["uuid"])
	}

	if name, ok := data["name"].(string); ok {
		self.Name = name
	} else {
		return NewBindError("job.Name", self.Name, data["name"])
	}

	return nil
}
