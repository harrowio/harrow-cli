package model

type Project struct {
	Uuid       string
	Name       string
	Attributes map[string]interface{}
	Jobs       []*Job
        Repositories []*Repository
}

func NewProject(subject map[string]interface{}) (*Project, error) {
	project := &Project{
		Attributes: subject,
	}

	if err := project.Bind(subject); err != nil {
		return nil, err
	} else {
		return project, nil
	}
}

func (self *Project) Bind(value interface{}) error {
	data, ok := value.(map[string]interface{})
	if !ok {
		return NewBindError("project", self, value)
	}

	if uuid, ok := data["uuid"].(string); ok {
		self.Uuid = uuid
	} else {
		return NewBindError("project.Uuid", self.Uuid, data["uuid"])
	}

	if name, ok := data["name"].(string); ok {
		self.Name = name
	} else {
		return NewBindError("project.Name", self.Name, data["name"])
	}

	return nil
}
