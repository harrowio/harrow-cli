package interactions

import (
	"reflect"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

func TestCreateOrganization_createsAnOrganization(t *testing.T) {
	be := backend.NewMock()
	url := "/organizations"
	organization := map[string]interface{}{
		"uuid":        "36d43242-2a56-4164-9f50-a50866e23850",
		"name":        "Test Organization",
		"public":      false,
		"githubLogin": "test-organization@example.com",
	}
	be.RespondTo("PUT", url, &backend.Response{
		Subject: organization,
	})

	args := &CreateOrganizationArguments{
		Name:        "Test Organization",
		Public:      false,
		GithubLogin: "test-organization@example.com",
	}
	ix := NewCreateOrganization(be)
	res := ix.Run(args)
	err := res.Error
	if err != nil {
		t.Fatal(err)
	}

	if got, want := res.Subject, organization; !reflect.DeepEqual(got, want) {
		t.Errorf("res.Subject = %#v\nwant: %#v\n", got, want)
	}
}

func TestCreateOrganization_validatesArguments(t *testing.T) {
	be := backend.NewMock()
	args := &CreateOrganizationArguments{}
	ix := NewCreateOrganization(be)
	res := ix.Run(args)
	err := res.Error
	if err == nil {
		t.Fatal("Expected an error")
	}

	verr, ok := err.(*ArgumentsError)
	if !ok {
		t.Errorf("err = %T; want %T", err, verr)
	}
}
