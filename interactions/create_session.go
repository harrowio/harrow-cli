package interactions

import "bitbucket.org/harrowio/harrow-cli/backend"

type CreateSessionArguments struct {
	Email    string
	Password string
}

func (self *CreateSessionArguments) Validate() error {
	err := NewArgumentsError()

	if self.Email == "" {
		err.Add("email", ArgumentRequired)
	}

	if self.Password == "" {
		err.Add("password", ArgumentRequired)
	}

	return err.ToError()
}

type CreateSession struct {
	backend backend.Interface
}

func NewCreateSession(backend backend.Interface) *CreateSession {
	return &CreateSession{backend: backend}
}

func (self *CreateSession) Run(args *CreateSessionArguments) *Result {
	if err := args.Validate(); err != nil {
		return NewFailure(err)
	}

	response := self.backend.Post("/sessions", args)

	return &Result{
		Subject: response.Subject,
		Error:   response.Error,
	}
}
