package interactions

import "strings"

type Embedder interface {
	Embed(rels []string)
	EmbedAll()
}

func Embed(ix Embedder, spec string) {
	if spec == "all" {
		ix.EmbedAll()
		return
	}

	rels := strings.Split(spec, ",")
	ix.Embed(rels)
}
