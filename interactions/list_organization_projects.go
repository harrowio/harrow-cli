package interactions

import (
	"fmt"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

type ListOrganizationProjectsArguments struct {
	OrganizationUuid string
}

func (self *ListOrganizationProjectsArguments) Validate() error {
	err := NewArgumentsError()

	if self.OrganizationUuid == "" {
		err.Add("organization.uuid", ArgumentRequired)
	}

	return err.ToError()
}

func (self *ListOrganizationProjectsArguments) Url() string {
	return fmt.Sprintf("/organizations/%s/projects", self.OrganizationUuid)
}

type ListOrganizationProjects struct {
	backend backend.Interface
}

func NewListOrganizationProjects(backend backend.Interface) *ListOrganizationProjects {
	return &ListOrganizationProjects{backend: backend}
}

func (self *ListOrganizationProjects) Run(args *ListOrganizationProjectsArguments) *Result {
	if err := args.Validate(); err != nil {
		return &Result{Error: err}
	}

	res := self.backend.Get(args.Url())
	return &Result{
		Collection: res.Collection,
		Error:      res.Error,
	}
}
