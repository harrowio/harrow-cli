package interactions

import "bitbucket.org/harrowio/harrow-cli/backend"

type CreateProjectArguments struct {
	Name             string `json:"name"`
	Public           bool   `json:"public"`
	OrganizationUuid string `json:"organizationUuid"`
}

func (self *CreateProjectArguments) Validate() error {
	err := NewArgumentsError()

	if self.Name == "" {
		err.Add("project.name", ArgumentRequired)
	}

	if self.OrganizationUuid == "" {
		err.Add("project.organizationUuid", ArgumentRequired)
	}

	return err.ToError()
}

type CreateProject struct {
	backend backend.Interface
}

func NewCreateProject(backend backend.Interface) *CreateProject {
	return &CreateProject{
		backend: backend,
	}
}

func (self *CreateProject) Run(args *CreateProjectArguments) *Result {
	if err := args.Validate(); err != nil {
		return &Result{Error: err}
	}

	res := self.backend.Post("/projects", args)
	if res.Error != nil {
		return &Result{Error: res.Error}
	}

	return &Result{
		Subject: res.Subject,
	}
}
