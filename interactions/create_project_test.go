package interactions

import (
	"reflect"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

func TestCreateProject_createsAProject(t *testing.T) {
	be := backend.NewMock()
	url := "/projects"
	project := map[string]interface{}{
		"uuid":             "36d43242-2a56-4164-9f50-a50866e23850",
		"organizationUuid": "75ee93cb-e9aa-403f-9e39-a91332c15440",
		"name":             "Test Project",
		"public":           false,
	}
	be.RespondTo("POST", url, &backend.Response{
		Subject: project,
	})

	args := &CreateProjectArguments{
		Name:             "Test Project",
		OrganizationUuid: "75ee93cb-e9aa-403f-9e39-a91332c15440",
		Public:           false,
	}
	ix := NewCreateProject(be)
	res := ix.Run(args)
	err := res.Error
	if err != nil {
		t.Fatal(err)
	}

	if got, want := res.Subject, project; !reflect.DeepEqual(got, want) {
		t.Errorf("res.Subject = %#v\nwant: %#v\n", got, want)
	}
}

func TestCreateProject_validatesArguments(t *testing.T) {
	be := backend.NewMock()
	args := &CreateProjectArguments{}
	ix := NewCreateProject(be)
	res := ix.Run(args)
	err := res.Error
	if err == nil {
		t.Fatal("Expected an error")
	}

	verr, ok := err.(*ArgumentsError)
	if !ok {
		t.Errorf("err = %T; want %T", err, verr)
	}
}
