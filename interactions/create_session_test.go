package interactions

import (
	"reflect"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

func TestCreateSession_postsToSessions(t *testing.T) {
	args := &CreateSessionArguments{
		Email:    "user@example.com",
		Password: "very-secure-password",
	}

	session := map[string]interface{}{
		"uuid":  "240a58a1-405a-474c-900a-fda3a20562b3",
		"valid": true,
	}
	be := backend.NewMock()
	be.RespondTo("POST", "/sessions", &backend.Response{
		Subject: session,
		Error:   nil,
	})

	ix := NewCreateSession(be)
	res := ix.Run(args)
	if err := res.Error; err != nil {
		t.Fatal(err)
	}

	if got, want := res.Subject, session; !reflect.DeepEqual(got, want) {
		t.Errorf("res.Subject = %#v; want %#v", got, want)
	}
}

func TestCreateSession_requiredArguments(t *testing.T) {
	tests := []string{"email", "password"}

	be := backend.NewMock()
	ix := NewCreateSession(be)

	args := &CreateSessionArguments{}
	for _, test := range tests {
		res := ix.Run(args)
		err := res.Error
		if err == nil {
			t.Fatal("Expected an error")
		}
		verr := err.(*ArgumentsError)

		if got, want := verr.Get(test), ArgumentRequired; got != want {
			t.Errorf("verr.Get(%q) = %q; want %q", test, got, want)
		}
	}
}
