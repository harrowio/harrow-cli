package interactions

import (
	"fmt"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

func TestListOrganizationProjects_getsProjects(t *testing.T) {
	be := backend.NewMock()
	organizationUuid := "d7ba79be-0a39-4609-ad33-9d29a08dda50"
	url := fmt.Sprintf("/organizations/%s/projects", organizationUuid)
	be.RespondTo("GET", url, &backend.Response{
		Collection: []map[string]interface{}{
			{
				"uuid": "f541469c-4613-488c-960d-ce5a27fdd79c",
				"name": "Project A",
			},
			{
				"uuid": "4946ef85-04be-4f2b-b6c4-6d577c0d6c9c",
				"name": "Project B",
			},
		},
	})

	ix := NewListOrganizationProjects(be)
	res := ix.Run(&ListOrganizationProjectsArguments{
		OrganizationUuid: organizationUuid,
	})
	err := res.Error
	if err != nil {
		t.Fatal(err)
	}

	if got, want := len(res.Collection), 2; got != want {
		t.Errorf("len(res.Collection) = %d; want %d", got, want)
	}
}

func TestListOrganizationProjects_validatesArguments(t *testing.T) {
	args := &ListOrganizationProjectsArguments{}
	ix := NewListOrganizationProjects(nil)
	res := ix.Run(args)
	err := res.Error
	if err == nil {
		t.Fatal("Expected an error")
	}

	verr := err.(*ArgumentsError)
	if got, want := verr.Get("organization.uuid"), ArgumentRequired; got != want {
		t.Errorf("err = %s; want %s", got, want)
	}
}
