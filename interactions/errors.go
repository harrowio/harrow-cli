package interactions

import (
	"bytes"
	"errors"
	"fmt"
)

const (
	ArgumentRequired = "required"
)

var (
	ErrLoginRequired = errors.New("login required")
)

type ArgumentsError struct {
	messages map[string]string
}

func NewArgumentsError() *ArgumentsError {
	return &ArgumentsError{
		messages: map[string]string{},
	}
}

func (self *ArgumentsError) Error() string {
	res := bytes.NewBufferString("interactions: arguments:")

	for field, msg := range self.messages {
		fmt.Fprintf(res, " %s[%s]", field, msg)
	}

	return res.String()
}

func (self *ArgumentsError) Get(field string) string {
	return self.messages[field]
}

func (self *ArgumentsError) Add(field, message string) {
	self.messages[field] = message
}

func (self *ArgumentsError) ToError() error {
	if len(self.messages) == 0 {
		return nil
	} else {
		return self
	}
}
