package interactions

import "bitbucket.org/harrowio/harrow-cli/backend"

type Show struct {
	backend backend.Interface
	path    string
	rels    func(*Result) []string
}

func NewShow(backend backend.Interface, path string) *Show {
	self := &Show{
		backend: backend,
		path:    path,
	}
	self.rels = self.noRels
	return self
}

func (self *Show) Embed(rels []string) {
	self.rels = func(*Result) []string { return rels }
}

func (self *Show) EmbedAll() {
	self.rels = self.allRels
}

func (self *Show) allRels(result *Result) []string {
	rels := []string{}
	for rel, _ := range result.Links {
		rels = append(rels, rel)
	}

	return rels
}

func (self *Show) noRels(result *Result) []string {
	return []string{}
}

func (self *Show) Run() *Result {
	res := self.backend.Get(self.path)
	if res.Error != nil {
		return &Result{Error: res.Error}
	}

	result := &Result{
		Subject:    res.Subject,
		Collection: res.Collection,
		Links:      res.Links,
	}

	for _, rel := range self.rels(result) {
		self.loadRelated(result, rel)
	}

	return result
}

func (self *Show) loadRelated(result *Result, rel string) {
	if rel == "self" || rel == "" {
		return
	}

	href := result.Links[rel]["href"]
	ix := NewShow(self.backend, href)
	if result.Embedded == nil {
		result.Embedded = map[string]*Result{}
	}
	result.Embedded[rel] = ix.Run()
}
