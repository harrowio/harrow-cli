package interactions

import (
	"fmt"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

func TestListUserOrganizations_returnsAListOfOrganizations(t *testing.T) {
	be := backend.NewMock()
	userUuid := "7905ef59-628d-4888-aa7f-d2979497f72c"
	url := fmt.Sprintf("/users/%s/organizations", userUuid)
	collection := []map[string]interface{}{
		{
			"uuid": "b6d290ab-a7c1-439f-878d-bd87652379df",
			"name": "Organization A",
		},
		{
			"uuid": "c7d5a8ef-ecb3-4bf1-9d6a-1ba8e4a257b5",
			"name": "Organization B",
		},
	}
	be.RespondTo("GET", url, &backend.Response{
		Collection: collection,
		Error:      nil,
	})

	ix := NewListUserOrganizations(be)
	result := ix.Run(&ListUserOrganizationsArguments{
		UserUuid: userUuid,
	})
	err := result.Error
	organizations := result.Collection
	if err != nil {
		t.Fatal(err)
	}

	if got, want := len(organizations), len(collection); got != want {
		t.Fatalf("len(organizations) = %d; want %d\n%#v\n", got, want, collection)
	}
}

func TestListUserOrganizations_requiredArguments(t *testing.T) {
	be := backend.NewMock()
	ix := NewListUserOrganizations(be)
	result := ix.Run(&ListUserOrganizationsArguments{
		UserUuid: "",
	})
	err := result.Error
	if err == nil {
		t.Fatal("Expected an error")
	}

	verr := err.(*ArgumentsError)
	if got, want := verr.Get("user.uuid"), ArgumentRequired; got != want {
		t.Errorf("verr.Get(%q) = %q; want %q", "user.uuid", got, want)
	}
}
