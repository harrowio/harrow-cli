package interactions

import "bitbucket.org/harrowio/harrow-cli/backend"

type Result struct {
	Subject    map[string]interface{}   `json:"subject,omitempty"`
	Collection []map[string]interface{} `json:"collection,omitempty"`
	Embedded   map[string]*Result       `json:"_embedded,omitempty"`
	Error      error                    `json:"error,omitempty"`
	Links      backend.Links            `json:"_links,omitempty"`
}

func NewFailure(err error) *Result {
	return &Result{
		Error: err,
	}
}
