package interactions

import (
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

func TestShow_returnsASubject(t *testing.T) {
	be := backend.NewMock()
	userUuid := "d00e4ae8-9c31-4207-98af-654b11002545"
	url := fmt.Sprintf("/users/%s/organizations", userUuid)
	user := map[string]interface{}{
		"uuid": userUuid,
		"name": "User Name",
	}
	be.RespondTo("GET", url, &backend.Response{
		Subject: user,
	})

	ix := NewShow(be, url)
	res := ix.Run()
	err := res.Error
	if err != nil {
		t.Fatal(err)
	}

	if got, want := res.Subject, user; !reflect.DeepEqual(got, want) {
		t.Errorf("res.Subject = %#v\nwant:\n%#v\n", got, want)
	}
}

func TestShow_afterCallToEmbed_fetchesGivenRelatedResources(t *testing.T) {
	be := backend.NewMock()
	userUuid := "d00e4ae8-9c31-4207-98af-654b11002545"
	url := fmt.Sprintf("/users/%s", userUuid)
	projectsUrl := fmt.Sprintf("/users/%s/projects", userUuid)
	user := map[string]interface{}{
		"uuid": userUuid,
		"name": "User Name",
	}
	projects := &Result{
		Subject: map[string]interface{}{
			"uuid": "cdf19cd9-3dc6-496d-b209-a471def02628",
			"name": "The Project",
		},
	}

	be.RespondTo("GET", url, &backend.Response{
		Subject: user,
		Links: backend.Links{
			"projects": backend.Link{
				"href": projectsUrl,
			},
		},
	})
	be.RespondTo("GET", projectsUrl, &backend.Response{
		Subject: projects.Subject,
	})

	ix := NewShow(be, url)
	ix.Embed([]string{"projects"})
	res := ix.Run()
	err := res.Error
	if err != nil {
		t.Fatal(err)
	}

	if got, want := res.Embedded["projects"], projects; !reflect.DeepEqual(got, want) {
		t.Errorf("res.Embedded[%q] = %#v\nwant:\n%#v\n", "projects", got, want)
	}
}

func TestShow_afterCallToEmbedAll_fetchesAllRelatedResources(t *testing.T) {
	be := backend.NewMock()
	userUuid := "d00e4ae8-9c31-4207-98af-654b11002545"
	url := fmt.Sprintf("/users/%s", userUuid)
	projectsUrl := fmt.Sprintf("/users/%s/projects", userUuid)
	user := map[string]interface{}{
		"uuid": userUuid,
		"name": "User Name",
	}
	projects := &Result{
		Subject: map[string]interface{}{
			"uuid": "cdf19cd9-3dc6-496d-b209-a471def02628",
			"name": "The Project",
		},
	}

	be.RespondTo("GET", url, &backend.Response{
		Subject: user,
		Links: backend.Links{
			"projects": backend.Link{
				"href": projectsUrl,
			},
		},
	})
	be.RespondTo("GET", projectsUrl, &backend.Response{
		Subject: projects.Subject,
	})

	ix := NewShow(be, url)
	ix.EmbedAll()
	res := ix.Run()
	err := res.Error
	if err != nil {
		t.Fatal(err)
	}

	if got, want := res.Embedded["projects"], projects; !reflect.DeepEqual(got, want) {
		t.Errorf("res.Embedded[%q] = %#v\nwant:\n%#v\n", "projects", got, want)
	}

}
