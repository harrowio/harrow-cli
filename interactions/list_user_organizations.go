package interactions

import (
	"fmt"

	"bitbucket.org/harrowio/harrow-cli/backend"
)

type ListUserOrganizations struct {
	backend backend.Interface
}

type ListUserOrganizationsArguments struct {
	UserUuid string
}

func (self *ListUserOrganizationsArguments) Validate() error {
	err := NewArgumentsError()

	if self.UserUuid == "" {
		err.Add("user.uuid", ArgumentRequired)
	}

	return err.ToError()
}

func (self *ListUserOrganizationsArguments) Url() string {
	if self.UserUuid == "" {
		return ""
	}
	return fmt.Sprintf("/users/%s/organizations", self.UserUuid)
}

func NewListUserOrganizations(backend backend.Interface) *ListUserOrganizations {
	return &ListUserOrganizations{
		backend: backend,
	}
}

func (self *ListUserOrganizations) Run(args *ListUserOrganizationsArguments) *Result {
	if err := args.Validate(); err != nil {
		return &Result{Error: err}
	}

	url := args.Url()
	response := self.backend.Get(url)

	return &Result{
		Collection: response.Collection,
		Error:      response.Error,
	}
}
