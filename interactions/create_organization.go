package interactions

import "bitbucket.org/harrowio/harrow-cli/backend"

type CreateOrganizationArguments struct {
	Name        string `json:"name"`
	Public      bool   `json:"public"`
	GithubLogin string `json:"githubLogin"`
}

func (self *CreateOrganizationArguments) Validate() error {
	err := NewArgumentsError()

	if self.Name == "" {
		err.Add("organization.name", ArgumentRequired)
	}

	return err.ToError()
}

type CreateOrganization struct {
	backend backend.Interface
}

func NewCreateOrganization(backend backend.Interface) *CreateOrganization {
	return &CreateOrganization{
		backend: backend,
	}
}

func (self *CreateOrganization) Run(args *CreateOrganizationArguments) *Result {
	if err := args.Validate(); err != nil {
		return &Result{Error: err}
	}

	res := self.backend.Put("/organizations", args)
	if res.Error != nil {
		return &Result{Error: res.Error}
	}

	return &Result{
		Subject: res.Subject,
	}
}
