//go:generate sh _tools/consts.sh
package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"

	"bitbucket.org/harrowio/harrow-cli/backend"
	"bitbucket.org/harrowio/harrow-cli/commands"
	"bitbucket.org/harrowio/harrow-cli/config"
	"bitbucket.org/harrowio/harrow-cli/view"

	"github.com/mitchellh/cli"
)

var (
	globalConfig = config.NewConfig()
	localConfig  = config.NewConfig()
	configParams = config.NewFirstMatch(
		localConfig,
		globalConfig,
	)
	globalArgs    = (*config.Arguments)(nil)
	defaultParams = (config.Getter)(nil)
	httpBackend   = (backend.Interface)(nil)

	ui = &cli.BasicUi{
		Reader:      os.Stdin,
		Writer:      os.Stdout,
		ErrorWriter: os.Stderr,
	}

	textView = view.NewText(ui)
	jsonView = view.NewJSON(os.Stdout)
	mainView = view.NewForFormat("txt", textView)
)

func init() {
	f, err := config.GlobalConfigFile()
	if err != nil {
		die(err)
	}
	if err := config.Load(globalConfig, f); err != nil {
		die(err)
	}

	f, err = config.LocalConfigFile()
	if err != nil {
		die(err)
	}
	if err := config.Load(localConfig, f); err != nil {
		die(err)
	}

	mainView.Add("json", jsonView)

	host := config.NewFirstMatch(
		config.NewEnvironment("HARROW"),
		configParams,
	).Get("host")

	persistHost := false
	if host == "" {
		host = config.NewFromUser(ui, ui).Get("host")
		persistHost = true
	}

	if hostURL, err := url.Parse(host); err != nil {
		ui.Error(fmt.Sprintf("%s: host(%q): %s", os.Args[0], host, err))
		os.Exit(1)
	} else if persistHost {
		hostURL.Scheme = "https"
		host = hostURL.String()
		globalConfig.Set("host", host)
		f, err := config.GlobalConfigFile()
		if err != nil {
			die(err)
		}
		if err := config.Save(globalConfig, f); err != nil {
			die(err)
		}
	}

	httpBackend = backend.NewHttp(host)
	httpBackend.SetHeader("User-Agent", USER_AGENT)
	sessionUuid := configParams.Get("session.uuid")
	if sessionUuid != "" {
		httpBackend.SetHeader("X-Harrow-Session-Uuid", sessionUuid)
	}

	globalFlags := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	globalFlags.Bool("json", false, "output as json")
	globalArgs = config.NewArguments(os.Args[0], globalFlags, []string{})

	defaultParams = config.NewFirstMatch(
		globalArgs,
		config.NewEnvironment("HARROW"),
		configParams,
	)
}

func die(err error) {
	ui.Error(err.Error())
	os.Exit(1)
}

func main() {
	c := cli.NewCLI("harrow-cli", "0.1.0")
	c.Args = os.Args[1:]
	c.Commands = map[string]cli.CommandFactory{
		"login":               loginCommandFactory,
		"list-organizations":  listOrganizationsCommandFactory,
		"create-organization": createOrganizationCommandFactory,
		"create-project":      createProjectCommandFactory,
		"show-organization":   showOrganizationCommandFactory,
		"show-project":        showProjectCommandFactory,
		"config":              configCommandFactory,
	}

	exitStatus, err := c.Run()
	if err != nil {
		log.Println(err)
	}

	os.Exit(exitStatus)
}

func loginCommandFactory() (cli.Command, error) {
	return commands.NewLogin(
		httpBackend,
		config.NewFirstMatch(
			config.NewFromUser(ui, ui),
		),
		localConfig,
		ui,
	), nil
}

func listOrganizationsCommandFactory() (cli.Command, error) {
	return commands.NewListOrganizations(
		httpBackend,
		configParams,
		ui,
	), nil
}

func createOrganizationCommandFactory() (cli.Command, error) {
	return commands.NewCreateOrganization(
		httpBackend,
		configParams,
		ui,
		ui,
	), nil
}

func createProjectCommandFactory() (cli.Command, error) {
	return commands.NewCreateProject(
		httpBackend,
		defaultParams,
		ui,
		ui,
	), nil
}

func showOrganizationCommandFactory() (cli.Command, error) {
	return commands.NewShow(
		httpBackend,
		commands.NewResource("organization", "organizations"),
		defaultParams,
		mainView,
	), nil
}

func showProjectCommandFactory() (cli.Command, error) {
	return commands.NewShow(
		httpBackend,
		commands.NewResource("project", "projects"),
		defaultParams,
		mainView,
	), nil
}

func configCommandFactory() (cli.Command, error) {
	return commands.NewConfig(
		localConfig,
		globalConfig,
		mainView,
	), nil
}
